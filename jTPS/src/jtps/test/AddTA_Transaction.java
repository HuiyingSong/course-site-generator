/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtps.test;

import jtps.jTPS_Transaction;
import tam.data.TAData;
import tam.data.TeachingAssistant;

/**
 *
 * @author 33570
 */
public class AddTA_Transaction implements jTPS_Transaction {
    private final TAData data;
    private final String newName;
    private final String newEmail;
    
    
    
    public AddTA_Transaction(TAData initData, String initName, String initEmail) {
        data = initData;
        newName = initName;
        newEmail = initEmail;
    }
    
    @Override
    public void doTransaction() {
        data.addTA(newName, newEmail);
    }
    
    @Override
    public void undoTransaction() {
        data.removeTA(newName);        
    }
}
