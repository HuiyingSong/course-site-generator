 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtps.test;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.beans.property.StringProperty;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import jtps.jTPS_Transaction;

import tam.data.TAData;
import tam.data.TeachingAssistant;
import tam.workspace.TAWorkspace;

/**
 *
 * @author 33570
 */
public class RemoveTA_Transaction implements jTPS_Transaction {
    private TAWorkspace workspace;
    private TeachingAssistant removedTA;
    private TAData data;
    private ArrayList<String> removedCells = new ArrayList<String>();
    
    
    public RemoveTA_Transaction(TAData initData, TeachingAssistant initTA, TAWorkspace initWorkspace) {
        data = initData;
        removedTA = initTA;
        workspace = initWorkspace;
    }
    
    @Override
    public void doTransaction() {
         TableView taTable = workspace.getTATable();
         String taName = removedTA.getName();
        data.removeTA(removedTA.getName());  
        HashMap<String, Label> labels = workspace.getOfficeHoursGridTACellLabels();
                for (String s : labels.keySet()) {
                    if (labels.get(s).getText().equals(taName)
                    || (labels.get(s).getText().contains(taName + "\n"))
                    || (labels.get(s).getText().contains("\n" + taName))) {
                        removedCells.add(s);
                        data.removeTAFromCell(labels.get(s).textProperty(), taName);
                    }
                }
    }
    
    @Override
    public void undoTransaction() {
        data.addTA(removedTA.getName(), removedTA.getEmail());

        for (String s : removedCells) {
            StringProperty cellProp = data.getOfficeHours().get(s);
            String cellText = cellProp.getValue();

            String taName = removedTA.getName();
            if (cellText.length() == 0) {
                cellProp.setValue(taName);
            } else {
                cellProp.setValue(cellText + "\n" + taName);
            }
        }

    }

    
}
