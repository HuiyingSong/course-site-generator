/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtps.test;

import jtps.jTPS_Transaction;
import tam.data.TAData;
import tam.data.TeachingAssistant;

/**
 *
 * @author 33570
 */
public class Toggle_Transaction implements jTPS_Transaction {
    private final TAData data;
    private final String cellKey;
    private final String taName;
    
    
    
    public Toggle_Transaction(TAData initData, String initCellKey, String initName) {
        data = initData;
        taName = initName;
        cellKey = initCellKey;
    }
    
    @Override
    public void doTransaction() {
        data.toggleTAOfficeHours(cellKey, taName);
    }
    
    @Override
    public void undoTransaction() {
        data.toggleTAOfficeHours(cellKey, taName);   
    }
}
