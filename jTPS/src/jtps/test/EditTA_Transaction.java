/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtps.test;
import djf.ui.AppMessageDialogSingleton;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.beans.property.StringProperty;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import jtps.jTPS_Transaction;
import properties_manager.PropertiesManager;
import static tam.TAManagerProp.MISSING_TA_NAME_AND_EMAIL_MESSAGE;
import static tam.TAManagerProp.MISSING_TA_NAME_AND_EMAIL_TITLE;
import static tam.TAManagerProp.TA_EMAIL_NOT_VALIDATE_MESSAGE;
import static tam.TAManagerProp.TA_EMAIL_NOT_VALIDATE_TITLE;
import static tam.TAManagerProp.TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE;
import static tam.TAManagerProp.TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE;

import tam.data.TAData;
import tam.data.TeachingAssistant;
import tam.workspace.TAWorkspace;

/**
 *
 * @author 33570
 */
public class EditTA_Transaction implements jTPS_Transaction {
    private TAWorkspace workspace;
    private TAData data;
    private ArrayList<String> editedCells = new ArrayList<String>();
    private String taName;
    private String name;
    private String email;
    
    
    public EditTA_Transaction(TAData initData, String initName, String newName, String initNewEmail, TAWorkspace initWorkspace) {
        data = initData;
        taName = initName;
        name = newName;
        email = initNewEmail;
        workspace = initWorkspace;
    }
    
    @Override
    public void doTransaction() {
        if (name != taName) {
            HashMap<String, Label> labels = workspace.getOfficeHoursGridTACellLabels();
                for (String s : labels.keySet()) {
                        if (labels.get(s).getText().equals(taName)
                                || (labels.get(s).getText().contains(taName + "\n"))
                                || (labels.get(s).getText().contains("\n" + taName))) {
                            editedCells.add(s);
                        data.editTAFromCell(labels.get(s).textProperty(), taName, name);
                        }
                }
        }
        data.editTA(taName, name, email);
    }

    @Override
    public void undoTransaction() {
        HashMap<String, Label> labels = workspace.getOfficeHoursGridTACellLabels();

        for (String s : editedCells) {
            if (labels.get(s).getText().equals(name)
                        || (labels.get(s).getText().contains(name + "\n"))
                        || (labels.get(s).getText().contains("\n" + name))) {
                    data.editTAFromCell(labels.get(s).textProperty(), name, taName);
                }
        }
        data.editTA(name, taName, email);
    }
}

    