/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtps.test;

import java.util.ArrayList;
import jtps.jTPS_Transaction;
import tam.data.TAData;
import tam.file.TimeSlot;
import tam.workspace.TAWorkspace;

/**
 *
 * @author 33570
 */
public class ComboBox_Transaction implements jTPS_Transaction{
    private final TAWorkspace workspace;
    private final TAData data;
    private final int startTime;
    private final int endTime;
    private final int initStart;
    private final int initEnd;
    private final ArrayList<TimeSlot> ts;
    
    
    public ComboBox_Transaction(TAData initData, int i, int j, TAWorkspace initWorkspace) {
        data = initData;
        startTime = i;
        endTime = j;
        initStart = data.getStartHour();
        initEnd = data.getEndHour();
        workspace = initWorkspace;
        ts = TimeSlot.buildOfficeHoursList(data);
    }
    
    @Override
    public void doTransaction() {
        workspace.resetWorkspace();
        data.initHours(String.valueOf(startTime), String.valueOf(endTime));

        for (TimeSlot t : ts) {
            String times = t.getTime();

            String[] s = times.split("_");
            int hour = Integer.parseInt(s[0]);
            if (s[1].contains("pm") && hour != 12) {
                hour += 12;
            }
            if (hour >= startTime && hour <= endTime) {
                String cellKey = data.getCellKey(t.getDay(), t.getTime());
                data.reloadToggle(cellKey, t.getName());
            }
        }
    }

    @Override
    public void undoTransaction() {

        workspace.resetWorkspace();
        data.initHours(String.valueOf(initStart), String.valueOf(initEnd));

        for (TimeSlot t : ts) {
            String times = t.getTime();
            String[] s = times.split("_");
            int hour = Integer.parseInt(s[0]);
            if (s[1].contains("pm") && hour != 12) {
                hour += 12;
            }
            String cellKey = data.getCellKey(t.getDay(), t.getTime());
            data.reloadToggle(cellKey, t.getName());

        }
    }

}
